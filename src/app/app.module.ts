import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { HomeComponent } from './components/home/home.component';
import { MainComponent } from './components/main/main.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { HomeEffects } from './components/home/home.component.effects';
import { HttpClientModule } from '@angular/common/http';
import { newsReducer } from './components/home/home.component.reducer';
import { ArticleComponent } from './components/article/article.component';
import { RegisterComponent } from './components/register/register.component';
import { programsReducer } from './components/register/register.component.reducer';
import { RegisterEffects } from './components/register/register.component.effects';
import { ReactiveFormsModule } from '@angular/forms';
//EffectsModule.forFeature([HomeEffects]),



@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    ArticleComponent,
    RegisterComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    NgbModule,
    ReactiveFormsModule,
    StoreModule.forRoot({}),
    StoreModule.forFeature('all', newsReducer),
    StoreModule.forFeature('all-programs', programsReducer),
    EffectsModule.forRoot([]),
    EffectsModule.forFeature([HomeEffects,RegisterEffects]),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
