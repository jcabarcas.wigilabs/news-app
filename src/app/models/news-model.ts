export interface New {
    id: string;
    title: string;
    body: string;
  }
  
  export interface News {
    list: New[];
  }