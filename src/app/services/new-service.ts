import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {  throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
@Injectable({ providedIn: 'root' })
export class NewService {
  

  url: string;
 
  constructor(private http: HttpClient) {
    this.url = `${environment.baseUrl}noticias`;
  }

  getAPINews() {
    return this.http.get(`${this.url}`)
      .pipe(catchError((error: any) => throwError(error.message)));
  }
}
