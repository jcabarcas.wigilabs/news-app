import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
@Injectable({ providedIn: 'root' })
export class RegisterService {

  constructor(private http: HttpClient) {
  }

  getAPIPrograms() {
    return this.http
      .get(`${environment.baseUrl}programas`)
      .pipe(catchError((error: any) => throwError(error.message)));
  }

  sendToUserAPI(action: any) {
    return this.http
      .post<any>(`${environment.baseUrl}registro`, action.payload)
      .pipe(catchError((error: any) => throwError(error.message)));
  }
}
