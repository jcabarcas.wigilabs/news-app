import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Store } from '@ngrx/store';
import * as RegisterActions from './register.component.actions';
import { Validators } from '@angular/forms';
import swal from'sweetalert2';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit {

  registerForm =this.formBuilder.group({
    name: ['',[Validators.required]],
    family_name: ['',Validators.required],
    email: ['',Validators.email],
    phone: ['',Validators.max(9999999999)],
    program: [''],
    comment: [''],
  });

  public programsList:  [{id:0,name:''}] =[{id:0,name:''}];

  constructor(private store: Store<any>,private formBuilder: FormBuilder) {}

  ngOnInit(): void {
    this.store.dispatch(new RegisterActions.GetAllPrograms());
    this.store.select('all-programs').subscribe(response => {
      if(response.programs.list.length>0){
        this.programsList=response.programs.list;
        this.registerForm.patchValue({
          program: this.programsList[0].id,
        });
      }
      
    }, error => {
      console.log(error);
    });
  }

  omit_special_char(e: { keyCode: any; which: any; })
{
   var k;
    document.all ? k = 
    e.keyCode : k = e.which;
    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57) || k==241);
}


  onSubmit() {
    let data=this.registerForm.value;
    data.phone=data.phone.toString();
    data.program=data.program.toString();
    this.store.dispatch(new RegisterActions.SendUser(data));
   this.store.select('all-programs').subscribe(response => {
      this.registerForm.reset();
      if(response.message=="OK"){
        swal.fire('Registro exitoso...', '', 'success');
      }
    }, error => {
      console.warn(error);
    }).unsubscribe;
  }
}

