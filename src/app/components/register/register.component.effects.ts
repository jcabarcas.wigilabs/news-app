import { Actions, createEffect, ofType } from "@ngrx/effects";
import { catchError, mergeMap, map, switchMap } from 'rxjs/operators';
import {  of } from 'rxjs';
import { RegisterActionType ,GetAllProgramsSuccess,GetAllProgramsFailed,SendUserFailed,SendUserSuccess} from './register.component.actions';
import { Injectable } from "@angular/core";
import { RegisterService } from "src/app/services/register-service";

@Injectable()
export class RegisterEffects {

  public loadPogramsList$  = createEffect(() =>
    this.actions$.pipe(
      ofType(RegisterActionType.GET_ALL_PROGRAMS),
      mergeMap(()=>this.registerService.getAPIPrograms().pipe(
        map(res =>
          new GetAllProgramsSuccess(res)
        ),
        catchError(err =>
          of(new GetAllProgramsFailed(err)))
      ))
      )
  );


  public sendUser$  = createEffect(() =>
  this.actions$.pipe(
    ofType(RegisterActionType.POST_USER),
    switchMap((action) => this.registerService.sendToUserAPI(action).pipe(
      map(res =>
        new SendUserSuccess(res)
      ),
      catchError(err =>
        of(new SendUserFailed(err)))
    ))
    )
);

  constructor(
    private actions$: Actions,
    private registerService: RegisterService
  ) { }
 
}