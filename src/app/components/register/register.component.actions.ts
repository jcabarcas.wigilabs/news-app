import { Action } from '@ngrx/store';


export enum RegisterActionType {
  GET_ALL_PROGRAMS = 'GET_ALL_PROGRAMS',
  GET_ALL_PROGRAMS_SUCCESS = 'GGET_ALL_PROGRAMS_SUCCESS',
  GET_ALL_PROGRAMS_FAILED = 'GET_ALL_PROGRAMS_FAILED',
  POST_USER = 'POST_USER',
  POST_USER_SUCCESS = 'POST_USER_SUCCESS',
  POST_USER_FAILED = 'POST_USER_FAILED',
}
 
export class GetAllPrograms implements Action {
  readonly type = RegisterActionType.GET_ALL_PROGRAMS;
}

export class GetAllProgramsSuccess implements Action {
  readonly type = RegisterActionType.GET_ALL_PROGRAMS_SUCCESS;  
  constructor(public payload: any) { }
}
 
export class GetAllProgramsFailed implements Action {
  readonly type = RegisterActionType.GET_ALL_PROGRAMS_FAILED;
  constructor(public payload: string) { }
}
 
export class SendUser implements Action {
  readonly type = RegisterActionType.POST_USER;
  constructor(public payload: any) { }
}

export class SendUserSuccess implements Action {
  readonly type = RegisterActionType.POST_USER_SUCCESS;  
  constructor(public payload: any) { }
}
 
export class SendUserFailed implements Action {
  readonly type = RegisterActionType.POST_USER_FAILED;
  constructor(public payload: string) { }
}
 
export type RegisterActions = GetAllPrograms |
GetAllProgramsSuccess |
GetAllProgramsFailed | SendUser |
SendUserSuccess |
SendUserFailed;
