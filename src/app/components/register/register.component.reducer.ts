import {
  RegisterActions,
  RegisterActionType,
} from './register.component.actions';

export const programsFeatureKey = 'programsList';

export interface ProgramState {
  programs: { list: [] };
}

export const initialState: ProgramState = {
  programs: { list: [] },
};


export function programsReducer(state = initialState, action: RegisterActions) {
  switch (action.type) {
    case RegisterActionType.GET_ALL_PROGRAMS: {
      return { ...state };
    }

    case RegisterActionType.GET_ALL_PROGRAMS_SUCCESS: {
      let result=[];
      let programsMap = action.payload.map((item: { name: any }) => {
        return [item.name, item];
      });
      var programsMapArr = new Map(programsMap);

      result = [...programsMapArr.values()];

      return {
        ...state,
        programs: { list: result },
      };
    }

    case RegisterActionType.GET_ALL_PROGRAMS_FAILED: {
      return { ...state };
    }
    case RegisterActionType.POST_USER: {
      console.log(action.payload);
      return { ...state ,message:"INIT"};
    }
    case RegisterActionType.POST_USER_FAILED: {
      return { ...state ,message:"FAIL"};
    }
    case RegisterActionType.POST_USER_SUCCESS: {
      return { ...state ,message:"OK"};
    }
    default:
      return {...state}
  }
}
