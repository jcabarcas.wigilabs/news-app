import { Actions, createEffect, ofType } from "@ngrx/effects";
import { catchError, mergeMap, map } from 'rxjs/operators';
import {  of } from 'rxjs';
import { NewService } from "src/app/services/new-service";
import { AllActionType ,GetAllSuccess,GetAllFailed} from './home.component.actions';
import { Injectable } from "@angular/core";

@Injectable()
export class HomeEffects {

  public loadNewsList$  = createEffect(() =>
    this.actions$.pipe(
      ofType(AllActionType.GET_ALL),
      mergeMap(()=>this.newService.getAPINews().pipe(
        map(res =>
          new GetAllSuccess(res)
        ),
        catchError(err =>
          of(new GetAllFailed(err)))
      ))
      )
  );

  constructor(
    private actions$: Actions,
    private newService: NewService
  ) { }
 
}