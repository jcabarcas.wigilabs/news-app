import { Action } from '@ngrx/store';


export enum AllActionType {
  GET_ALL = 'GET_ALL',
  GET_ALL_SUCCESS = 'GET_ALL_SUCCESS',
  GET_ALL_FAILED = 'GET_ALL_FAILED'
}
 
export class GetAll implements Action {
  readonly type = AllActionType.GET_ALL;
}

export class GetAllSuccess implements Action {
  readonly type = AllActionType.GET_ALL_SUCCESS;  
  constructor(public payload: any) { }
}
 
export class GetAllFailed implements Action {
  readonly type = AllActionType.GET_ALL_FAILED;
  constructor(public payload: string) { }
}
 
export type AllActions = GetAll |
  GetAllSuccess |
  GetAllFailed;
