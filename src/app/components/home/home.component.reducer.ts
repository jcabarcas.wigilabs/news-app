
import { News } from 'src/app/models/news-model';
import { AllActions, AllActionType } from './home.component.actions';

export const newsFeatureKey = 'newsList';

export interface NewState {
  news: News;
}

export const initialState: NewState = {
  news: { list: [] },
};

export function newsReducer(state = initialState, action: AllActions) {
  switch (action.type) {
 
    case AllActionType.GET_ALL: {
      return { ...state };
    }
 
    case AllActionType.GET_ALL_SUCCESS: {
      let msgText = '';
      let bgClass = '';
 
      if (action.payload.length < 1) {
        msgText = 'No data found';
        bgClass = 'bg-danger';
      } else {
        msgText = 'Loading data';
        bgClass = 'bg-info';
      }
 
      return {
        ...state,
        news: {list:action.payload},
        message: msgText,
        infoClass: bgClass
      };
    }
 
    case AllActionType.GET_ALL_FAILED: {
      return { ...state };
    }
  }
}

