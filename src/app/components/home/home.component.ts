import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { New } from 'src/app/models/news-model';
import * as HomeActions from './home.component.actions';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  public newsList$: New[] | undefined;
  p = 1;
  constructor(private store: Store<any>,private router :Router) { }

  ngOnInit(): void {
    this.store.dispatch(new HomeActions.GetAll());
    this.store.select('all').subscribe(response => {
      if(response!=undefined && response.news.list.length>0){
        this.newsList$=response.news.list;
      }
    }, error => {
      console.log(error);
    });
  }

  details(id : String):void{
    this.router.navigate(['/noticias', id]);
  }

}
