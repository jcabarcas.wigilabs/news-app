import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { New } from 'src/app/models/news-model';
import * as HomeActions from '../home/home.component.actions';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css'],
})
export class ArticleComponent implements OnInit {
  public id: String | null | undefined;
  public newSelected: New | undefined;
  p = 1;
  constructor(private route: ActivatedRoute, private store: Store<any>) {}

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    this.store.dispatch(new HomeActions.GetAll());
    this.store.select('all').subscribe(
      (response) => {
        if (response.news.list.length > 0) {
          for (const key in response.news.list) {
            if (Object.prototype.hasOwnProperty.call(response.news.list, key)) {
              const element = response.news.list[key];
              if (element.id == this.id) {
                this.newSelected = element;
                break;
              }
            }
          }
          if (this.newSelected == undefined) {
            window.location.href = '/home';
          }
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }
}
